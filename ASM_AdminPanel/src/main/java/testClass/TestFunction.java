package testClass;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import Ultis.JpaUtils;
import dao.VideoDao;
import model.Video;

public class TestFunction {
	
	private static EntityManager em = JpaUtils.getEntityManager();

	protected void finalize() throws Throwable {
		em.close();
		super.finalize();
	}

	public static void main(String[] args) {
		// Create a new instance of VideoDAO
		VideoDao videoDao = new VideoDao();

		// Get the video data
		List<Object[]> videoData = videoDao.getVideoData();

        for (Object[] row : videoData) {
            int id = (int) row[0];
            String title = (String) row[1];
            int views = (int) row[2];
            long likeCount = (long) row[3];
            long shareCount = (long) row[4];
            long downloadCount = (long) row[5];

            System.out.println("ID: " + id);
            System.out.println("Title: " + title);
            System.out.println("Views: " + views);
            System.out.println("Likes: " + likeCount);
            System.out.println("Shares: " + shareCount);
            System.out.println("Downloads: " + downloadCount);
            System.out.println();
        }
		// Close the EntityManagerFactory
		JpaUtils.shutDown();
	}
}
