package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DownloadDao;
import dao.FavoriteDAO;
import dao.ShareDAO;
import dao.VideoDao;

@WebServlet("")
public class IndexController extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		findAll(req, resp);
		getAll(req, resp);
		req.getRequestDispatcher("/index.jsp").forward(req, resp);
	}
	
	protected void findAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		VideoDao videoDao = new VideoDao();
		List<Object[]> videoData = videoDao.getVideoData();
		
		request.setAttribute("listVideo", videoDao);
		
		
	}

	protected void getAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			FavoriteDAO dao = new FavoriteDAO();
			request.setAttribute("likeamount", dao.getCount());
			
			ShareDAO dao2 = new ShareDAO();
			request.setAttribute("shareamount", dao2.getCount());
			
			DownloadDao dao3 = new DownloadDao();
			request.setAttribute("buyamount", dao3.getCount());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doPost(req, resp);
	}
}
