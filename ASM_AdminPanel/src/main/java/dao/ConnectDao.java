package dao;

import javax.persistence.EntityManager;

import Ultis.JpaUtils;

public class ConnectDao {

	public static final EntityManager entityManager = JpaUtils.getEntityManager();

	@SuppressWarnings("deprecation")
	@Override
	protected void finalize() throws Throwable {
		entityManager.close();
		super.finalize();
	}
}
