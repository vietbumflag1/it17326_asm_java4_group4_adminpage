package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.hibernate.sql.Insert;

import Ultis.JpaUtils;
import model.Favorite;
import model.User;

public class FavoriteDAO extends ConnectDao {
	private EntityManager em = JpaUtils.getEntityManager();

	@Override
	protected void finalize() throws Throwable {
		em.close();
		super.finalize();
	}

	public List<Favorite> findAll() {
		String jpql = "Select o from Favorite o";
		// TypedQuery<Favorite> query = em.createQuery(jpql, Favorite.class);
		List<Favorite> result = em.createQuery("from Favorite", Favorite.class).getResultList();
		return result;
	}

	public List<Favorite> getFavoriteByUserId(int userId) {
	    String jpql = "FROM Favorite WHERE user.id = :userId";
	    TypedQuery<Favorite> query = em.createQuery(jpql, Favorite.class);
	    query.setParameter("userId", userId);
	    return query.getResultList();
	}

	public void createFavorite(Favorite favorite) {
		try {
			em.getTransaction().begin();
			em.persist(favorite);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		}
	}
	public long getCount() {
	    String jpql = "SELECT COUNT(o) FROM Share o";
	    TypedQuery<Long> query = em.createQuery(jpql, Long.class);
	    return query.getSingleResult();
	}
	
}
