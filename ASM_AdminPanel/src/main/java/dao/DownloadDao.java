package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.hibernate.sql.Insert;

import Ultis.JpaUtils;
import model.Favorite;

public class DownloadDao extends ConnectDao{
	private EntityManager em = JpaUtils.getEntityManager();

	@Override
	protected void finalize() throws Throwable {
		em.close();
		super.finalize();
	}

	public List<Favorite> findAll() {
		String jpql = "Select o from Download o";
		List<Favorite> result = em.createQuery("from Download", Favorite.class).getResultList();
		return result;
	}
	
	public long getCount() {
	    String jpql = "SELECT COUNT(o) FROM Download o";
	    TypedQuery<Long> query = em.createQuery(jpql, Long.class);
	    return query.getSingleResult();
	}
}
