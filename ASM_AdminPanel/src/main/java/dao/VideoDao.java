package dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.hibernate.Query;

import Ultis.JpaUtils;
import model.Video;

public class VideoDao extends ConnectDao {
	private EntityManager em = JpaUtils.getEntityManager();

	protected void finalize() throws Throwable {
		em.close();
		super.finalize();
	}

	public Video create(Video entity) {
		try {
			em.getTransaction().begin();

			em.persist(entity);// them

			em.getTransaction().commit();
			return entity;
		} catch (Exception e) {

			em.getTransaction().rollback();
			throw new RuntimeException(e);
			// TODO: handle exception
		}
	}

	// lấy hết video lên
	public List<Video> findAll() {
		String jpql = "Select o from Video o";
		TypedQuery<Video> query = em.createQuery(jpql, Video.class);
		return query.getResultList();
	}

	public List<Video> findbyDate() {
		String jpql = "Select o from Video o ORDER BY o.date DESC";
		TypedQuery<Video> query = em.createQuery(jpql, Video.class);
		return query.getResultList();
	}

	public Video findById(int id) {
		return em.find(Video.class, id);
	}

	public Video remove(int id) {
		try {
			em.getTransaction().begin();
			Video entity = this.findById(id);
			em.remove(entity);
			em.getTransaction().commit();
			return entity;
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new RuntimeException(e);
			// TODO: handle exception
		}
	}

	public void updateVideoViews(int videoId) {
		try {
			Video video = em.find(Video.class, videoId);
			video.setViews(video.getViews() + 1);
			em.merge(video);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Object[]> getVideoData() {
        EntityManager em = JpaUtils.getEntityManager();
        javax.persistence.Query query = em.createQuery("SELECT v.id, v.title, v.views, COUNT(l.id) as likeCount, COUNT(s.id) as shareCount, COUNT(d.id) as downloadCount "
                + "FROM Video v "
                + "LEFT JOIN v.likes l "
                + "LEFT JOIN v.shares s "
                + "LEFT JOIN v.downloads d "
                + "GROUP BY v.id, v.title, v.views");
        List<Object[]> videoData = query.getResultList();
        em.close();
        return videoData;
    }


}
