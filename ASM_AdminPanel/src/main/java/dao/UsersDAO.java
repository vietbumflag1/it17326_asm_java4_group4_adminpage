package dao;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.hibernate.Session;

import Ultis.JpaUtils;
import model.User;

public class UsersDAO extends ConnectDao {
	private EntityManager em = JpaUtils.getEntityManager();

	protected void finalize() throws Throwable {
		em.close();
		super.finalize();
	}

	public List<User> findAll() {
		String jpql = "Select o from Users o";
		TypedQuery<User> query = em.createQuery(jpql, User.class);
		return query.getResultList();
	}
	public User getUserByEmailAndPassword(String email, String password) {
	    try {
	    	String jpql = "FROM Users WHERE email = :email AND password = :password";
	        TypedQuery<User> query = em.createQuery(jpql, User.class);
	        query.setParameter("email", email);
	        query.setParameter("password", password);
	        return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
}
