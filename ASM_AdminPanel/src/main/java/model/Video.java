package model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Video database table.
 * 
 */
@Entity
@Table(name = "Video")
@NamedQuery(name="Video.findAll", query="SELECT v FROM Video v")
public class Video implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="Id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name="Active")
	private boolean active;

	@Column(name="Description")
	private String description;

	@Column(name="NgayDang")
	private Date ngayDang;

	@Column(name="Poster")
	private String poster;

	@Column(name="Price")
	private int price;

	@Column(name="Title")
	private String title;

	@Column(name="VideoLink")
	private String videoLink;

	@Column(name="Views")
	private int views;
	
	@OneToMany(mappedBy = "video", cascade = CascadeType.ALL, orphanRemoval = true)
    List<Favorite> likes;

    @OneToMany(mappedBy = "video", cascade = CascadeType.ALL, orphanRemoval = true)
    List<Share> shares;

    @OneToMany(mappedBy = "video", cascade = CascadeType.ALL, orphanRemoval = true)
    List<Download> downloads;
	
	public Video() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Video(int id, boolean active, String description, Date ngayDang, String poster, int price, String title,
			String videoLink, int views) {
		super();
		this.id = id;
		this.active = active;
		this.description = description;
		this.ngayDang = ngayDang;
		this.poster = poster;
		this.price = price;
		this.title = title;
		this.videoLink = videoLink;
		this.views = views;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getNgayDang() {
		return ngayDang;
	}

	public void setNgayDang(Date ngayDang) {
		this.ngayDang = ngayDang;
	}

	public String getPoster() {
		return poster;
	}

	public void setPoster(String poster) {
		this.poster = poster;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getVideoLink() {
		return videoLink;
	}

	public void setVideoLink(String videoLink) {
		this.videoLink = videoLink;
	}

	public int getViews() {
		return views;
	}

	public void setViews(int views) {
		this.views = views;
	}

	public List<Favorite> getLikes() {
		return likes;
	}

	public void setLikes(List<Favorite> likes) {
		this.likes = likes;
	}

	public List<Share> getShares() {
		return shares;
	}

	public void setShares(List<Share> shares) {
		this.shares = shares;
	}

	public List<Download> getDownloads() {
		return downloads;
	}

	public void setDownloads(List<Download> downloads) {
		this.downloads = downloads;
	}
	
	


	
}