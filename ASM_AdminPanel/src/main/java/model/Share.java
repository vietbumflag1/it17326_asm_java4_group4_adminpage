package model;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.*;


/**
 * The persistent class for the Share database table.
 * 
 */
@Entity
@Table(name = "Share", uniqueConstraints = {
		@UniqueConstraint(columnNames = {"VideoId", "UserId"})
})
@NamedQuery(name="Share.findAll", query="SELECT s FROM Share s")
public class Share implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UserId")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VideoId")
    private Video video;

    @Column(name = "Emails")
    private String emails;

    @Column(name = "ShareDate", nullable = false)
    private LocalDate shareDate;

	public Share(int id, User user, Video video, String emails, LocalDate shareDate) {
		super();
		this.id = id;
		this.user = user;
		this.video = video;
		this.emails = emails;
		this.shareDate = shareDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}

	public String getEmails() {
		return emails;
	}

	public void setEmails(String emails) {
		this.emails = emails;
	}

	public LocalDate getShareDate() {
		return shareDate;
	}

	public void setShareDate(LocalDate shareDate) {
		this.shareDate = shareDate;
	}

    
}