package model;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.*;


/**
 * The persistent class for the Download database table.
 * 
 */
@Entity
@Table(name = "Download", uniqueConstraints = {
		@UniqueConstraint(columnNames = {"VideoId", "UserId"})
})
@NamedQuery(name="Download.findAll", query="SELECT d FROM Download d")
public class Download implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UserId")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VideoId")
    private Video video;

    @Column(name = "DownloadDate", nullable = false)
    private LocalDate downloadDate;
    

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}

	public LocalDate getDownloadDate() {
		return downloadDate;
	}

	public void setDownloadDate(LocalDate downloadDate) {
		this.downloadDate = downloadDate;
	}

	public Download(int id, User user, Video video, LocalDate downloadDate) {
		super();
		this.id = id;
		this.user = user;
		this.video = video;
		this.downloadDate = downloadDate;
	}

	
}