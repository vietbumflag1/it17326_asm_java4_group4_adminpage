package model;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.*;

import org.hibernate.mapping.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * The persistent class for the Favorite database table.
 * 
 */
@Entity
@Table(name = "Favorite", uniqueConstraints = {
		@UniqueConstraint(columnNames = {"VideoId", "UserId"})
})
@NamedQuery(name="Favorite.findAll", query="SELECT f FROM Favorite f")
public class Favorite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UserId")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VideoId")
    private Video video;

    @Column(name = "LikeDate", nullable = false)
    private LocalDate likeDate;
	
	
    // "SELECT o.video FROM FAVORITE o "
	public Favorite() {
		super();
		// TODO Auto-generated constructor stub
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public Video getVideo() {
		return video;
	}


	public void setVideo(Video video) {
		this.video = video;
	}


	public LocalDate getLikeDate() {
		return likeDate;
	}


	public void setLikeDate(LocalDate likeDate) {
		this.likeDate = likeDate;
	}


	public Favorite(int id, User user, Video video, LocalDate likeDate) {
		super();
		this.id = id;
		this.user = user;
		this.video = video;
		this.likeDate = likeDate;
	}

	

}