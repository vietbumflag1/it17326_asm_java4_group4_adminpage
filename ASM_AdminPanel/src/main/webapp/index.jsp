<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Dashboard - NiceAdmin Bootstrap Template</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
    <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  
    <style type="text/css">
    	<jsp:include page="/views/assets/css/style.css" />
  		<jsp:include page="/views/assets/vendor/bootstrap/css/bootstrap.min.css" />
  		<jsp:include page="/views/assets/vendor/bootstrap-icons/bootstrap-icons.css" />
  		<jsp:include page="/views/assets/vendor/boxicons/css/boxicons.min.css" />
  		<jsp:include page="/views/assets/vendor/quill/quill.snow.css" />
  		<jsp:include page="/views/assets/vendor/quill/quill.bubble.css" />
  		<jsp:include page="/views/assets/vendor/simple-datatables/style.css" />
  		<jsp:include page="/views/assets/vendor/remixicon/remixicon.css" />
  		<jsp:include page="/views/assets/css/style.css" />
	</style>

  <!-- =======================================================
  * Template Name: NiceAdmin
  * Updated: Mar 09 2023 with Bootstrap v5.2.3
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <jsp:include page="/views/includedFolder/Header.jsp" />

  <!-- ======= Sidebar ======= -->
  <jsp:include page="/views/includedFolder/Aside.jsp" />
  <!-- End Sidebar-->

  <jsp:include page="/views/IndexMain.jsp" />
  <!-- End #main -->

  <!-- ======= Footer ======= -->
  <!-- End Footer -->
  <jsp:include page="/views/includedFolder/Footer.jsp" />

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>


</body>

</html>