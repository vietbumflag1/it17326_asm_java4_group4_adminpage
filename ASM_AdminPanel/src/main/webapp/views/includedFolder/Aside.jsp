<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

      <li class="nav-item">
        <a class="nav-link " href="index.html">
          <i><img alt="" src="https://cdn-icons-png.flaticon.com/128/3596/3596982.png" style="height: 20px;"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->

      <li class="nav-heading">Pages</li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="users-profile.html">
          <i><img alt="" src="https://cdn-icons-png.flaticon.com/128/1077/1077114.png" style="height: 20px;"></i>
          <span>Profile</span>
        </a>
      </li><!-- End Profile Page Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" href="pages-faq.html">
          <i><img alt="" src="https://cdn-icons-png.flaticon.com/128/984/984199.png" style="height: 20px;"></i>
          <span>F.A.Q</span>
        </a>
      </li><!-- End F.A.Q Page Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" href="pages-contact.html">
          <i><img alt="" src="https://cdn-icons-png.flaticon.com/128/1034/1034153.png" style="height: 20px;"></i>
          <span>Contact</span>
        </a>
      </li><!-- End Contact Page Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" href="pages-register.html">
          <i><img alt="" src="https://cdn-icons-png.flaticon.com/128/2910/2910756.png" style="height: 20px;"></i>
          <span>Register</span>
        </a>
      </li><!-- End Register Page Nav -->


    </ul>

  </aside>