<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt" %>

<main id="main" class="main">

    <section class="section dashboard">
    
      <div class="row">
      
      <div class="col-lg-8">
          <div class="row">
            <!-- Top Selling -->
            <div class="col-4">
              <div class="card top-selling overflow-auto">

                <div class="filter">
                  <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                </div>

                <div class="card-body pb-0">
                  <h5 class="card-title">Video <span>| Lượt thích</span></h5>
					<h5 class="card-title">${likeamount}</h5>

                </div>

              </div>
            </div><!-- End Top Selling -->
            <div class="col-4">
              <div class="card top-selling overflow-auto">

                <div class="filter">
                  <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                </div>

                <div class="card-body pb-0">
                  <h5 class="card-title">Video <span>| Lượt chia sẻ</span></h5>
					<h5 class="card-title">${shareamount}</h5>

                </div>

              </div>
            </div><!-- End Top Selling -->
            <div class="col-4">
              <div class="card top-selling overflow-auto">

                <div class="filter">
                  <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                </div>

                <div class="card-body pb-0">
                  <h5 class="card-title">Video <span>| Lượt mua</span></h5>
					<h5 class="card-title">${buyamount}</h5>

                </div>

              </div>
            </div><!-- End Top Selling -->

          </div>
          
          <div class="row">
            <!-- Top Selling -->
            <div class="col-12">
              <div class="card top-selling overflow-auto">

                <div class="filter">
                  <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                </div>

                <div class="card-body pb-0">
                  <h5 class="card-title">Video <span>| Mới</span></h5>

                  <table class="table table-borderless">
                    <thead>
                      <tr>
                        <th scope="col">Hình</th>
                        <th scope="col">Tiêu đề</th>
                        <th scope="col">Like</th>
                        <th scope="col">Share</th>
                        <th scope="col">Mua</th>
                      </tr>
                    </thead>
                    <tbody>
                    
						<c:forEach var="video" items="${listVideo}">
	                      <tr>
	                        <th scope="row"><a href="#"><img src="data:image/jpg;base64,{video.poster}" width="20" height="35"/></a></th>
	                        <td><a href="#" class="text-primary fw-bold">${video.title}</a></td>
	                        <td>${video.likeCount}</td>
					        <td class="fw-bold">${video.shareCount}</td>
					        <td>${video.downloadCount}</td>
					        <td class="fw-bold">${video.views}</td>
	                      </tr>
						</c:forEach>
                    </tbody>
                  </table>

                </div>

              </div>
            </div><!-- End Top Selling -->

          </div>
        </div><!-- End Left side columns -->
        
        <!-- Right side columns -->
        <div class="col-lg-4">
          <!-- News & Updates Traffic -->
          <div class="card">

            <div class="card-body pb-0">
              <h5 class="card-title">Video <span>| Mới Nhất</span></h5>

              <div class="news">
                <div class="post-item clearfix">
                  
                  	<div class="card pt-4">
					  <h4><a href="#"><img src="data:image/jpg;base64,${newest.image}" width="360" height="240"/></a></h4>
					  <div class="card-body">
					    <h5 class="card-title">{newst.title}</h5>
					    <p class="card-text">{newst.desc}</p>
					    <ul>
								<li style="font-size: 20px"><i class="fa fa-star"></i> 1</li>
								<li style="font-size: 20px"><i
									class="fa-solid fa-paper-plane"></i> 2</li>
								<li style="font-size: 20px"><i class="fa fa-download"></i>
									23</li>
							</ul>
					    <a href="#" class="btn btn-primary">Chỉnh sửa Video</a>
					  </div>
					</div>
                </div>

              </div><!-- End sidebar recent posts-->

            </div>
          </div><!-- End News & Updates -->

        </div><!-- End Right side columns -->

      </div>
    </section>

  </main>